#pragma once

#include <stdint.h>
#include <stdio.h>

#define BMP_HEADER_SIZE 54


#pragma pack(push, 1)
struct BmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct BMP {
    struct BmpHeader bmp_header;
    uint8_t** image;
    uint8_t* color_table;
};

void read_header(FILE* file, struct BmpHeader* header);
void write_header(FILE* file, const struct BmpHeader* header);

uint8_t* read_line(FILE* file, size_t length);
void write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width);
void write_image(FILE* file, const struct BMP* image);
