#include "bmp_structure.h"

#include <stdlib.h>

void read_header(FILE* file, struct BmpHeader* header) {
    fread(header, 1, BMP_HEADER_SIZE, file);
}

void write_header(FILE* file, const struct BmpHeader* header) {
    fwrite(header, BMP_HEADER_SIZE, 1, file);
}

uint8_t* read_line(FILE* file, size_t length) {
    uint8_t* line = malloc(length);
    fread(line, 1, length, file);
    if (length % 4 != 0) {
        uint8_t padding_buffer[4 - length % 4];
        fread(padding_buffer, 1, 4 - length % 4, file);
    }
    return line;
}

void write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width) {
    const uint8_t padding_length = 4 - width % 4;
    uint8_t* padding = NULL;
    if (padding_length != 0) {
        padding = malloc(padding_length);
        for (int i = 0; i < padding_length; ++i) {
            padding[i] = 0;
        }
    }
    for (size_t i = height - 1; i > 0; --i) {
        fwrite(data[i], 1, width, file);
        if (padding_length != 0) {
            fwrite(padding, 1, padding_length, file);
        }
    }
    fwrite(data[0], 1, width, file);
    if (padding_length != 0) {
        fwrite(padding, 1, padding_length, file);
    }
    if (padding != NULL) {
        free(padding);
    }
}

void write_image(FILE* file, const struct BMP* image) {
    write_header(file, &(image->bmp_header));
    if (image->color_table != NULL) {
        fwrite(image->color_table, 1, image->bmp_header.bOffBits - BMP_HEADER_SIZE, file);
    }
    write_bitmap(file, (const uint8_t**) image->image, image->bmp_header.biHeight,
                 image->bmp_header.biWidth * (image->bmp_header.biBitCount / 8));
}
