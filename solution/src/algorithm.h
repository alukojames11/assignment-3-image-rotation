#pragma once

#include "bmp_structure.h"

#include "stdlib.h"

struct BMP load_and_rotate_image(FILE* input) {
    struct BMP rotated_image;
    struct BmpHeader base_header;
    read_header(input, &base_header);

    if (base_header.bOffBits != BMP_HEADER_SIZE) {
        rotated_image.color_table = malloc(base_header.bOffBits - BMP_HEADER_SIZE);
        fread(rotated_image.color_table, 1, base_header.bOffBits - BMP_HEADER_SIZE, input);
    } else {
        rotated_image.color_table = NULL;
    }

    rotated_image.bmp_header = base_header;

    rotated_image.bmp_header.biWidth = base_header.biHeight;
    rotated_image.bmp_header.biHeight = base_header.biWidth;

    const size_t color_size = ((size_t)base_header.biBitCount / 8);
    const size_t base_bytes_length = (size_t)base_header.biWidth * color_size;
    const size_t rotated_bytes_length = (size_t)rotated_image.bmp_header.biWidth * color_size;

    rotated_image.image = malloc(sizeof(uint8_t*) * rotated_image.bmp_header.biHeight);
    for (uint32_t i = 0; i < rotated_image.bmp_header.biHeight; ++i) {
        rotated_image.image[i] = malloc(rotated_bytes_length);
    }

    for (uint32_t i = 0; i < base_header.biHeight; ++i) {
        uint8_t* line = read_line(input, base_bytes_length);
        for (uint32_t j = 0; j < base_bytes_length; j += color_size) {
            for (uint32_t k = 0; k < color_size; ++k) {
                rotated_image.image[rotated_image.bmp_header.biHeight - j / color_size - 1]
                                    [rotated_bytes_length - i * color_size - color_size + k] = line[j + k];
            }
        }
        free(line);
    }

    rotated_image.bmp_header.biSizeImage =  rotated_image.bmp_header.biHeight * (rotated_image.bmp_header.biWidth * color_size + (4 - (rotated_image.bmp_header.biWidth * color_size) % 4) % 4);
    rotated_image.bmp_header.bfileSize = BMP_HEADER_SIZE +
            base_header.bOffBits - BMP_HEADER_SIZE +
            rotated_image.bmp_header.biSizeImage;

    return rotated_image;
}

