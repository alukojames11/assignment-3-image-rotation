#include <stdio.h>

#include "algorithm.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Not enough arguments: expected 3, got %i", argc);
        return -1;
    }
    FILE* input = fopen(argv[1], "rb");
    FILE* output = fopen(argv[2], "wb");
    if (input == NULL) {
        printf("Wrong input file!");
        return -1;
    }
    if (output == NULL) {
        printf("Wrong output file!");
        return -1;
    }

    struct BMP rotated_image = load_and_rotate_image(input);
    write_image(output, &rotated_image);

    for (int i = 0; i < rotated_image.bmp_header.biHeight; ++i) {
        free(rotated_image.image[i]);
    }
    free(rotated_image.image);
    if (rotated_image.color_table != NULL) {
        free(rotated_image.color_table);
    }

    fclose(input);
    fclose(output);
    return 0;
}
